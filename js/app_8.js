"use strict";

const paragraph = document.body.getElementsByTagName("p");
for (let i of paragraph) {
  i.style.backgroundColor = "#ff0000";
}

const opList = document.querySelector("#optionsList");
console.log(opList);
console.log(opList.parentElement);
if (opList.hasChildNodes()) {
  console.log(opList.childNodes);
}

const testParagraph = (document.querySelector("#testParagraph").innerHTML =
  "This is a paragraph");

const mainHeader = document.querySelector(".main-header").children;
for (let i of mainHeader) {
  i.classList.add("nav-item");
}
console.log(mainHeader);

const sectionTitle = document.querySelectorAll(".section-title");
for (let i of sectionTitle) {
  i.classList.remove("section-title");
}
